#include <libsocket/libinetsocket.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////
//This is a function we created to handle the editing of the array elements.

void strreplace(char *string, const char *find, const char *replacW)
{
    if(strstr(string, replacW) != NULL)
     {
        char *tmpstr = malloc(strlen(strstr(string, find) + strlen(find)) + 1);
        
        strcpy(tmpstr, strstr(string, find) + strlen(find));    //Create a string with what's after the replaced part
        
        *strstr(string, find) = '\0';    //Take away the part to replace and the part after it in the initial string
        
        strcat(string, replacW);    //Concat the first part of the string with the part to replace with
        
        strcat(string, tmpstr);    //Concat the first part of the string with the part after the replaced part
        
        free(tmpstr);    //Free the memory to avoid memory leaks
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
           // http://api.wunderground.com/api/1655f919bbcd29ed/conditions/q/ZIPCODE.json
           
   int fileSocket = create_inet_stream_socket("api.wunderground.com", "80", LIBSOCKET_IPv4, 0);
  
    if(fileSocket == -1)
      {
          
        printf("Can't connect\n");
        
        exit(1);
        
      }
      
  FILE *file = fdopen(fileSocket, "rb+");
  
  if(!file)
       {
           
          printf("Can't convert fileSocket to FILE\n");
        exit(1);
        
       }
       
       
  fprintf(file, "GET /api/1655f919bbcd29ed/conditions/q/%s.json HTTP/1.0\n", argv[1]);
  
  fprintf(file, "Host: api.wunderground.com\n");
  
  fprintf(file, "\n");
  
  
  
  char wLines[1000];
  
  printf("Current Conditions\n");
    
  char *outputs[] = {"observation_time\":", "full", "temp_f", "relative_humidity", "wind_dir", "wind_mph" };
  
  char copiedEdits[7][100];
  
  int k = 0;
    
  while(fgets(wLines, 1000, file) != NULL)
      {
          
         if (wLines[strlen(wLines)-1] == '\n')
         {
             
             wLines[strlen(wLines)-1] = '\0';
             
         } 
         
        for ( int i = 0; i < 6; i++)
         {
             
          char *results = strstr(wLines, outputs[i]);
          
          if (results != NULL)
             {
                 
               strcpy(copiedEdits[i], results);
                k++;
                break;
                
             }
          }
     }
///////////////////////////////////////////////////////////////////////////////
// Now to edit the elements of the copiedEdits array
//Array element 0
    strreplace(copiedEdits[0], "observation_time\":\"", "");
    
         strreplace(copiedEdits[0], "\",", "");
        
//Array  element 1
    strreplace(copiedEdits[1], "full\":\"", "");
    
         strreplace(copiedEdits[1], "\",", "");
        
//Array  element 2
    strreplace(copiedEdits[2], "temp_f\":", "");
    
         strreplace(copiedEdits[2], ",", "");
        
//Array  element 3
     strreplace(copiedEdits[3], "relative_humidity\":\"", "");
     
         strreplace(copiedEdits[3], "\",", "");
        
//Array  element 4
     strreplace(copiedEdits[4], "wind_dir\":\"", "");
     
         strreplace(copiedEdits[4], "\",", "");
        
//Array  element 5
    strreplace(copiedEdits[5], "wind_mph\":", "");
    
         strreplace(copiedEdits[5], ",", "");
     
    printf("Observation Time: %s\n", copiedEdits[0]);
    
    printf("Location: %s\n", copiedEdits[1]);
    
    printf("Temperature: %s F\n", copiedEdits[2]); 
    
    printf("Humidity: %s\n", copiedEdits[3]);
    
    printf("Wind: %s %s mph\n", copiedEdits[4], copiedEdits[5]);
///////////////////////////////////////////////////////////////////////////////
    
    fclose(file);
 
    destroy_inet_socket(fileSocket);
 
}